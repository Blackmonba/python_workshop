### Lab 18 by Anthony Blackmon
###1
from cImage import *
oldpixel=Pixel(0,255,0)
def negativePixel(oldpixel):
        newred = 255-oldpixel.getRed()
        newgreen=255-oldpixel.getGreen()
        newblue=255-oldpixel.getBlue()
        newPixel=Pixel(newred,newgreen,newblue)
        return newPixel
def remgreen():
        pixel=Pixel(255,0,255)
        myWin = ImageWin("image Processing",400,400)
        Yoshi = FileImage('wsu.gif')     
        Yoshi.draw(myWin)
        width=Yoshi.getWidth()
        height=Yoshi.getHeight()
        newim=EmptyImage(width,height)
        for row in range(height):
                for col in range(width-row,col,pixel):
                        originalPixel = Yoshi.getPixel(col,row)
                        newPixel = negativePixel(originalPixel)
                        newim.setPixel(col,row,newPixel)
        newim.setPosition(height,height)
        newim.draw(myWin)
        newim.save("newim.gif")
#2
from  cImage import *
import cImage
myWin = ImageWin("image Processing",400,400)
lineImage=EmptyImage(400,400)
redpixel=(255,0,0)
for i in range(400):
    lineImage.setPixel(i ,200, redpixel)
lineImage.draw(myWin)
##    Yoshi = FileImage('Yoshi.gif')     
##    Yoshi.draw(myWin)

