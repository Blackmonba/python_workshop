class Phone:

    """ this is the constructor
        for my phone class"""
    def __init__(self, manufacturer, model, retail, quantity):
        self.manufact = manufacturer
        self.model = model
        self.retail_price = retail
        self.quantity = quantity

    """set up mutator methods for class"""
    
    def set_manufact(self, manufacturer):
        self.manufact = manufacturer

    def set_model(self,model):
        self.model = model

    def set_retail_price(self,retail):
        self.retail_price = retail

    def set_quantity(self, quantity):
        self.quantity = quantity

    """set up getter methods to call on data from class"""

    def get_manufact(self):
        return self.manufact

    def get_model(self):
        return self.model

    def get_retail_price(self):
        return self.retail_price

    def get_quantity(self):
        return self.quantity

""" this is my store class """

class Store(Phone):

    def __init__(self, manufacturer, model, retail_price, quantity):

         self.phones = []
         self.number_of_phones = 0
         Phone.__init__(self, manufacturer, model, retail_price, quantity)
         
     
    """adds phone instance to store inventory"""
    def add_Phone(self,Phone):
        if Phone not in self.phones:
            self.phones.append(Phone)
            self.number_of_phones += 1
        else:
            self.number_of_phones += 1


    """removes instance of phones from store inventory"""

    def remove_Phone(self):
        y=1
        n=0
        a=input('are you sure you want to delete this phone y or n >>')
        if y:
            print(self.manufact,self.model,self.retail_price)
            self.number_of_phones -= 1
        else:
            pass
            
    """ to return the amount of phones"""
    def get_number_of_phones(self):
        return self.number_of_phones


    def update_phone_info(self):
        print("Please choose an option to update.")
        print('[0] manufacturer')
        print('[1] model')
        print('[2] retail')
        print('[3] qunatitiy')
        print('[4] EXIT ')
        user_choice=input('> ')
        
        if user_choice== "1":
            x=input(str('enter new manufacturer'))
            self.manufact=x
            
        elif user_choice=="2":
            y=input(str("enter new model"))
            self.model=y
        elif user_choice== "3":
            z=input(str('enter adjusted retail price'))
            self.retail_price=z

        elif user_choice== "4":
            w=input(str("enter the quantity wanted"))
            self.quantity=w
        elif  user_choice=="5":
            print ('thank you goodbye')
            pass
        else:
            pass
