# Python Lab 19 by Anthony B.
#1
from cImage import *
import cImage
##myWin=ImageWin("WSU",400,400)
oldimage=FileImage('wsu.gif')
##oldimage.draw(myWin)
def double(oldimage):
    myWin=ImageWin("WSU",400,400)
    oldw=oldimage.getWidth()
    oldh=oldimage.getHeight()
    newim = EmptyImage(oldw*2,oldh*2)
    for row in range(newim.getHeight()):
        for col in range(newim.getWidth()):
            originalCol=col//2
            originalRow=row//2
            oldpixel=oldimage.getPixel(originalCol,originalRow)
            newim.setPixel(col,row,oldpixel)
    newim.draw(myWin)
    oldimage.draw(myWin)
    return newim
#2
import cTurtle
aturtle=cTurtle.Turtle()

def recKoch(aTurtle, distance, level):
	print("level: %3d distance: %d" %(level, distance))
	if level == 0:
		aTurtle.forward(distance)
	else:
		recKoch(aTurtle, distance/3, level-1)
		aTurtle.left(60)
		recKoch(aTurtle, distance/3, level-1)
		aTurtle.right(120)
		recKoch(aTurtle, distance/3, level-1)
		aTurtle.left(60)
		recKoch(aTurtle, distance/3, level-1)

#basically this function is doing a loop within a loop it goes over the distance /3 and after it through each sub itteration it goes back to the beginning and repeats its self until level == 0

#3
def GCD(a,b):
        d = a%b
        if d==0:
            print(d)
        else:
            if d>=0:
               print(d%d)
                
    
#4                
def func(aString):
    if aString == '':
        return ''
    else:
        return func(aString[1:])+ aString[0]
#this function reversing the order of the string    
