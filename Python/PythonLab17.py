#1
from cImage import*
myWin = ImageWin('Line Image:',300,300)
lineImage = EmptyImage(300,300)
import random
redPixel=Pixel(255,0,0)
randomPixel=Pixel(random.randint(0,256),random.randint(0,256),random.randint(0,256))
for i in range(0,300):
        lineImage.setPixel(50,i,redPixel)
        lineImage.draw(myWin)
for i in range(50,250):
        lineImage.setPixel(150,i,randomPixel)
        lineImage.draw(myWin)
myWin.exitOnClick()
    
#2
x1=int(input("Enter a number for x1:"))
y1=int(input("Enter a number for y1:"))
x2=int(input("Enter a number for x2:"))
y2=int(input("Enter a number for y2:"))
from cImage import*
myWin = ImageWin('Line Image:',300,300)
lineImage = EmptyImage(300,300)
import random
randomPixel=Pixel(random.randint(0,256),random.randint(0,256),random.randint(0,256))
for i in range(x1,x2):
     lineImage.setPixel(x1,i,randomPixel)
     lineImage.draw(myWin)
for i in range(y1,y2):
     lineImage.setPixel(y1,i,randomPixel)
     lineImage.draw(myWin)
myWin.exitOnClick()

#3
import cImage
def negativePixel(oldpixel):
        newred = 255-oldpixel.getRed()
        newgreen=255-oldpixel.getGreen()
        newblue=255-oldpixel.getBlue()
        newPixel=cImage.Pixel(newred,newgreen,newblue)
        return newPixel
def remgreen():
        myWin = cImage.ImageWin("image Processing",400,400)
        Yoshi = cImage.FileImage('wsu.gif')     
        Yoshi.draw(myWin)
        width=Yoshi.getWidth()
        height=Yoshi.getHeight()
        newim=cImage.EmptyImage(width,height)
        for row in range(height):
                for col in range(width):
                        originalPixel = Yoshi.getPixel(col,row)
                        newPixel = negativePixel(originalPixel)
                        newim.setPixel(col,row,newPixel)
        newim.setPosition(width+1,0)
        newim.draw(myWin)
