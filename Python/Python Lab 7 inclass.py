# Python In Class 7 by Anthony Blackmon

# Question 1
alph = "abcdefghijklmnopqrstuvwxyz "
def rot13(plainText, key):        
    key = "nopqrstuvwxyzabcdefghijklm "
    plainText = plainText.lower()
    cipherText = ""
    for i in plainText:
        index = alph.find(i)
        cipherText = cipherText + key[index]
    return cipherText

# Question 2
data = "myprogram.exe"
print(data[3])
print(data[-2])
print(len(data))
print(data[0:7])
print(data[2:6])
newData = data.replace('exe', '')
print(newData)
print(data[int(len(newData.replace('exe',''))/2)])
print(newData.replace(data[int(len(newData)/2)], ' '))

# Question 3
def reverse():
    a = input('Enter your message:')
    b = a[::-1]
    print(b)
    
