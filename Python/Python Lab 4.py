

# Python lab 4 by Anthony Blackmon

# Question 1

a = True or False
print(a)

# Question 2

b = False or True

print(b)

#Question 3
c = not 27>2

print(c)

# Question 4

d = not(True and False)

print(d)


# Question 5

e = (not True)and(not False)

print(e)

# Question 6
g = (not(True and False))

print(g)

#Question 7
x = 4

x<=1 or x<=10

print(True)

#Question 8

import math

def archimedes(sides):
    innerangleB = 360.0/sides
    halfangleA =innerangleB/2
    onehalfsideS = math.sin(math.radians(halfangleA))
    sideS = onehalfsideS * 2
    polygonCircumference = sides * sideS
    pi = polygonCircumference/2

for i in range(8,100,8):
    print(archimedes)


# Question 9

a,b = 0, 1
while b < 59:
    a, b = b, a+b 
    print(a+b)
