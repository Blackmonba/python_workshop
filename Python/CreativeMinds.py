"""Anthony Blackmon
   Creative Programs
   July 11 2012"""

"Common Python Errors"
"""
SyntaxError: invalid syntax
ImportError: No module named random
SyntaxError: EOL while scanning string literal
AttributeError: "str" object has no attribute lower
IndentationError: expected an indented block
IndentationError: unexpected indent
IndentationError: unindented does not match any outer indent
TypeError: bad operand type for abs(): str
TypeError: abs() takes exactly one argument
IndexError: list index out of range
KeyError: spam
"""
class MyGUI:
	def __init__self(self):
		#Create the main window widget.
		self.main_window=tkinter.Tk()
		#Create a label widget.
		self.label=tkinter.Label(self.main_window, text='Hello World')
		#Call the Label widget's pack method.
		self.label.pack()
		#Enter the tkinter main loop.
		tkinter.mainloop()
my_gui=MyGUI()
