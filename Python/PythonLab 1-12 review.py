# Python Lab 1-12 review by Anthony Blackmon

#Lab 1
print('Lab 1')
#1a
a = 13+8+25
print(a)

#1b
a = 42*12*31
print (a)
#1c
# Number of Min in a yr
a = 60 # 60 = how many minutes in a hour
b = 24 # 24 = how many hours in a day
c = 365 # 365 = how many days in a year
d = a*b*c
print('The number of minutes in a year is:',d)

#1d
# Number of hours in a century
a = 100 # how many years is a century
b = 365 # how many days in a year
c = 24 # how many hours in a day
d = a*b*c
print('The number of hours in a century is:',d)

#1e
# Compute the number of 9 square foot tiles to cover a 27x36 foot room.
a = 27*36
b = 9
c = a//b
print('The number of tiles to cover the room is:',c)

#1f
#Find Average
a = (9,12,14,18,30)
b = len(a)
c = sum(a)//b
print(c)

#1g
a = 30*20*16 # deminsion of feet
b = 8 # cubic feet
c = a//b
print('The number of boxes that will fit the truck is:', c)

#1h
a = 125 # 5 Quarters
b = 20  # 2 Dimes
c = 40  # 8 Nickels
d = a+b+c # Total
print(d)

#2a
GPA = (4.0,3.78,2.9,3.2,2.88,3.9,2.7) # GPA of 7 Students
Average = sum(GPA)//len(GPA) # Takes total of GPA divided by length of GPA
print(Average)

#2b
#40,000/12/7 prints 476.19

#2c
a = 125 # 5 Quarters
b = 20  # 2 Dimes
c = 40  # 8 Nickels
d = a+b+c # Total
print(float(d))

#2d,e,f,g
#Compute Exponent
a = 2.5**3.1
b = -2.5**3.1
c = 2.5**-3.1
d = -2.5**-3.1
print(a)
print(b)
print(c)
print(d)

#Lab 2
print('Lab 2')

#1a
a = 8
b = 75
c = a*b
print(c)

#1b
a = 4
b = 6
c = a*b
a = a*b
b = c+2
c = a*b**3/c
print(a)
print(b)
print(c)

#1c
a = 4
b = 9
c = a**b
b = b**b
a= b%7
print(a)
print(b)
print(c)

#3a
for i in range(4,35,2):
    print(i)

#3b
for i in range (3,31,3):
    print(i)
#3c
for i in range (-7,7,2):
    print (i)
# Lab 3
print('Lab 3')

#Q1
a = 'My name is Anthony Blackmon.'
b = 'I was born 10/05/88.'
print (a,b)

#Q2
def name(a):
    a = input('Enter your first name:')
    print(a)
    b = input('Enter your last name:')
    print ('Hello', a,b)
#Q3
def height():
    a = int(input("Enter your height without inches:"))
    feet2inches = a*12
    print("Your height is:",feet2inches)
#Q6
    a = 'Python'.center(20)
    b = a.upper()
    print(b)
# Lab 4
print('Lab 4')
#1
a = True or False
print(a)
#2
b = False and True
print(b)
#3
c = not 27>2
print(c)
#4
d = not(True and False)
print(d)
#5
e = (not True) and (not False)
print (e)
#6
f = (not(True and False))
print(f)
#7
def Boo(x):
 x == 0
 if x<= 10 or x<=1:
     print(True)
 else:
     print(False)

#9
a,b = 0,1
if True:# change if to while
    a,b = b,a+b
    print (a+b)

    
print('Lab 4 TakeHome')

#1
def factorial(n):
     if n==0:
       return 1
     else:
       return n*factorial(n-1)
#2
def average(n):
     sum = 0
     count = 0
     for i in range(1,100,2):
         sum=sum+n
         count=count+1
         ave = sum//count
         return average
#9
def larnum(a,b,c):
    a = int(input('Enter a #:'))
    b = int(input('Enter a #:'))
    c = int(input('Enter a #:'))
    if c<a and a>b:
            return a
    if c<b and c>a:
            return b
    if c>a and c>b:
            return c

#Lab 5
print('Lab 5')

#1
print('Enter a double digit #')
def GPA(x):
   if x >= 90.0:
       print('4.0')
   elif x >= 80.0:
        print('3.0')
   elif x >= 70.0:
        print('2.0')
   elif x >= 60.0:
        print('1.0')
   else:
        print('0')

#2
def leapyear(year):
    if year % 4 ==0:
        return True
    elif year % 100 ==0:
        return True
    elif year % 400 ==0:
        return True
    else:
        return False
# Lab 6
#1a
print('Lab 6')
Fname = 'Anthony'
Lname = 'Blackmon'
print(Fname,Lname)
#b
Fname[0:8]
#c
Lname[0:8]
#d
print(Lname,Fname)
#e
len(Lname)

#2a
state = 'mississippi'
print(state)
#b
a = state.count('s')
print(a)
#c
b = state.replace('iss','ox')
print(b)
#d
c = state.index('p')
print(c)

#('Lab 7)
print('Lab 7')
 #1

 #2a
data = 'myprogram.exe'
print(data)
a = data[3]
print(a)
b = data[-2]
print(b)
c = len(data)
print(c)
d = data[0:7]
print(d)
#2b
e = data[2:6]
print(e)
f = data.replace('exe', '')
print(f)
g = f
print(g)
print(data[int(len(g.replace('exe',''))/2)])
 # Python Lab 8 Take Home by Anthony Blackmon

def f(n):
    if n == 0: return 0
    elif n == 1: return 1
    else: return f(n-1)+f(n-2)

    print (n)
    
# Question 1

def intval(number):
    ascII = 48
    order = ord(number)
    value=order-ascII
    print(value)


    
   
# Question 2
def lettertoIndex(ch):
    idx = ord(ch)
    if idx > 0:
        print (idx)

#Question 3

def indextoLetter(ch):
    idx=chr(ch)
    if ch < 97:
        print('Not A Character')
    else:
      print (idx) 



#Question 4

def gpa(x):
    if x>=95:
        print ('A')
    elif x<=94 and x>=90:
            print ('A-')
    elif x<=89 and x >=88:
            print ('B+')
    elif x<=87 and x >=83:
            print ('B')
    elif x<=82 and x>=80:
            print ('B-')
    elif x<=79 and x>= 78:
            print('C+')
    elif x<=77 and x>=73:
            print('C')
    elif x<=72 and x>=70:
            print('C-')
    elif x<=69 and x>=68:
            print ('D+')
    elif x<=67 and x>=63:
            print ('D')
    elif x<=62 and x>=60:
            print('D-')
    elif x<=59 and x>=0:
            return ('F')
# Python Lab 9 & 10 by Anthony Blackmon

#Question 1
# A)
myList = [9, 4, 'c', 'elephant', True]
print(myList)

myList.append(3.14)
myList.append(7)
print(myList)


#B)
myList[5]= 'cat'
print(myList)

#C)
print(myList.index('elephant'))

#D)
a = myList.count('9')
print(a)

#E)
del myList[0]
print(myList)

#F)
myList.pop(4)
print(myList)

# Question 2
string= 'the quick brown fox'
b = string.split()
print (b)

# Question 3
string2 = 'mississippi'
b= string2.split('ss')
print(b)

# Question 4
#4a
def counter(mylist,y):
    counter=0
    for x in mylist:
        if x == y:
            counter=counter+1

    return counter
#4b
#def search(string,letter):
##    n = 0
##    if y =(len(string)):
##        if string[i]==letter:
##            n=n+1
##            print(True)
##        else:
##            print (False)



#4c
def reverse():
    a = input('Enter your message:')
    b = a[::-1]
    print(b)
    

#4d


#4e
def insert(alist):
    a = ""
    for i in alist(a, alist[:]):
        return a




#Question 5
print('Create a list called Mylist then run shuffle(Mylist)')
def shuffle(Mylist):
    import random
    random.shuffle(Mylist)
    print (Mylist)

#Question 6
def getMin(alist):
    maxSoMin = alist[0]
    for item in alist[:-1]:
        while item < maxSoMin:
            maxSoMin = item
    return maxSoMin





    
    
# Python Lab 11 by Anthony Blackmon

#1
def Sumtotal(alist):
    sum = 0
    for i in range(0,len(alist)):
        sum=sum+alist[i]
    return sum


#2

Ages = [21,30,27,34,22,26,19,31,30]
print(Ages)
mean = sum(Ages)/len(Ages)
print(mean)


#3
Ages = [21,30,27,34,22,26,19,31,30]
def median (Ages):
    print(Ages)
    newlist = Ages
    newlist.sort()
    pos = len(newlist)//2
    print (Ages[pos])
    
#4
NOS=[30,45,20,19,10]
print(NOS)
mean = sum(NOS)/len(NOS)
print(mean)

#5
name = ['joe','tom','barb','sue','sally']
scores = [10,23,13,18,12]
def update(name,scores):
    a=dict(zip(name,scores))
    scoreDict=a
    newscore=scoreDict['sally']=13
    print('scoreDict:',scoreDict)
    print (scoreDict.get('barb'))
    print (list(scores))
    print (scoreDict.get('sally'))
    
# Python Lab 12 by Anthony Blackmon

# Question 1

Students = ['Amy','Joey','Nick','Tony','Juvi']
print(Students)
Score = [85,75,65,99,89]
print(Score)
b = (['Amy', Score[0], 'Joey', Score[1], 'Juvi',Score[4],'Nick',Score[2],'Tony',Score[3]])
print (b)


# Question 2
dictionary={85,75,65,99,89}
name = ['Amy','Joey','Nick','Tony','Juvi']
def getScore(name, dictionary):
    a= input('Enter a name:')
    dictionary={}
    dictionary['Amy']=85
    dictionary['Joey']=75
    dictionary['Juvi']=89
    dictionary['Nick']=65
    dictionary['Tony']=99
    if a not in dictionary:
         print('Error: ')
         return int(-1)
    else:
        print (dictionary.get(str(a),dictionary))
# Question 3
print('#3')
b = [('john', 10),('bob', 8),('john', 5),('bob',17)]
a = {'john':[10,5],'bob':[8,17]}
b = a
print('Run avename(a)')
def avename(a):
       for i in a:
           c = sum(a.get('john'))/len(a.get('john'))
           print ('john:', c)
           d = sum(a.get('bob'))/len(a.get('bob'))
           print ('bob:', d)


#alt 3
           alist = [('john', 10),('bob', 8),('john', 5),('bob',17)]
def avename(alist):
    count = {}
    name = []
    value = {}
    name2 = ()
    for i in range(len(alist)):
        name2 = alist[i]
        if name2[0] not in name:
            name.append(name2[0])
            value[name2[0]]=name2[1]
            count[name2[0]]=1
        else:
            value[name2[0]]=value[name2[0]]+name2[1]
            count[name2[0]]=count[name2[0]]+1
        for i in name:
            print(i,name[i]/name2[i])

# Question 4
mylist=[(2,3),(7,3),(6,1),(8,1),(2,3),(4,3),(1,1),(8,2)]
def TheFreqTab(mylist):
    mylist.sort()
    print('item','frequencey')
    countlist= []
    previous= mylist[0]
    groupcount= 0
    
    for current in  mylist:
        if current == previous:
            groupcount= groupcount+1
            previous = current
        else:
            print(previous,' ',groupcount)
            previous = current
            groupcount = 1
    print(current,' ',groupcount)        
                        
           





