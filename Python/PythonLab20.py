# Python Lab 20 by Anthony Blackmon
#1

import cTurtle
aTurtle=cTurtle.Turtle()
def drawSquare(aTurtle, side):
    for i in range(3):
        	aTurtle.forward(side)
        	aTurtle.right(90)
        	aTurtle.forward(side)
        	
        	

def nestedBox(aTurtle, side):
    if side >= 1:                    
        	drawSquare(aTurtle,side)    
        	nestedBox(aTurtle,side-5) 

#2
import cTurtle
t=cTurtle.Turtle()
def tree(trunkLength,t):
    if trunkLength < 5:    #base case     
        return
    else:
         t.forward(trunkLength)
         t.color("green")
         t.right(30)
         tree(trunkLength-15,t) #first recursive call to draw smaller tree on right side of trunk
         t.left(60)
         tree(trunkLength-15,t)
         t.right(30)
         t.backward(trunkLength)
         t.color("brown")


#3
depth = 5
import cTurtle
t=cTurtle.Turtle()
myTurtle=t
p1=(100,200)
p2=(75,250)
p3=(50,300)
def Triangle(t,p1,p2,p3):
    t.up()
    t.goto(p1)
    t.down()
    t.goto(p2)
    t.goto(p3)
    t.goto(p1)

def midPoint(p1, p2):
    return ((p1[0]+p2[0])/2.0,(p1[1]+p2[0])/2.0)

def sierpinski(myTurtle,p1, p2, p3,depth):
            if depth > 0:
                sierpinski(myTurtle, p1,midpoint(p1,p2),midPoint(p1,p3),depth-1,)
                sierpinski(myTurtle, p1,midpoint(p2,p3),midPoint(p2,p1),depth-1,)
                sierpinski(myTurtle, p1,midpoint(p3,p1),midPoint(p3,p2),depth-1,)
            else:
                    drawTriangle(myTurtle,p1,p2,p3)

            
