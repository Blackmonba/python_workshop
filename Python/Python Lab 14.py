# Python Lab 14 by Anthony Blackmon
#1a
amount = 61.235
print ('Your salary is $ %0.2f' % amount) #it is r-justified and it starts at 0 the value 2 is the umber after the decimal
print("The area is %0.1f"%amount)# it is r_justified but it reads one value after the decimal and starts from 0
print('%7f' % amount)# The width is 7 so it added zeros
# 1b
x = 2
y = 3
z = 4
print(('%-6d' %x),('%-6d'%y),('%-6d' %z))

#1c
salaries = [1,2,3,4,5]
for i in range(len(salaries)):
    print("%+12.2f" %salaries[i])


#2
header = ("City  " " Rainfall")
print(header.center(24))
infile = open("rainfall.txt",'r')
outfile = open("rainfallfmt.txt",'w')
headers= infile.readline(0)
for aline in infile:
    values = aline.split()
    a = str(values[0])
    inches = float(values[1])
    cm = 2.54*inches
    b = (cm)
    print("%+10s %7.2fcm" %(a,b))
    outfile.write(values[0]+" "+str(cm)+"\n")

infile.close()
outfile.close()

#3
Line ,Words = 0,0
file = open('rainfall.txt','r')
for eachline in file:
    line=len(eachline.split())*len(eachline.split())
    Words = line
    for eachline in file:
                a = file.seek(0)
                b = file.read()
                c = len(b)
                print("Lines:", line)
                print("Words:",Words)
                print("Characters:", c)
              


