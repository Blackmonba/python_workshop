# Lab 2 Take Home by Anthony Blackmon

#1)
def drawTriangle(myTurtle, sideLength):
    for i in range(3):
        myTurtle.forward(sideLength)
        myTurtle.right(120)

#2)def drawSpiral(myTurtle, maxSide):
    for sideLength in range (1,maxSide+1,5):
        myTurtle.forward(sideLength)
        myTurtle.right(90)
